package com.example.skantext;

import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MainActivity extends Activity {

	final int galleryRequest = 1;
	Bitmap galleryPic = null;
	private Uri mImageUri;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final EditText lang = (EditText) findViewById(R.id.lang);
		final EditText psm = (EditText) findViewById(R.id.psm);
		final int galleryRequest = 1;
		ImageButton openPhoto = (ImageButton) findViewById(R.id.btnOpenPhoto);
		openPhoto.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
				photoPickerIntent.setType("image/*");
				startActivityForResult(photoPickerIntent, galleryRequest);

			}
		});

		final ImageButton btnStart = (ImageButton) findViewById(R.id.btn_start);

		btnStart.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v)

			{
				Intent servicdIntent = new Intent(MainActivity.this,
						UploadPic.class);
				Log.d("MainActivityUri", mImageUri.toString());
				servicdIntent.putExtra(UploadPic.KEY_IMAGE_URL,
						mImageUri.toString());
				servicdIntent.putExtra("lang", lang.toString());
				servicdIntent.putExtra("psm", psm.toString());
				startService(servicdIntent);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
		ImageView myImageView = (ImageView) findViewById(R.id.image);
		galleryPic = null;
		Intent intent;
		switch (requestCode) {
		case galleryRequest:
			if (resultCode == RESULT_OK) {
				mImageUri = imageReturnedIntent.getData();
				try {
					galleryPic = Media.getBitmap(getContentResolver(),
							mImageUri);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				myImageView.setImageBitmap(galleryPic);
			}
		}
	}
}
