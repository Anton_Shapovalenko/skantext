package com.example.skantext;

import java.io.File;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;

public class UploadPic extends IntentService {

	public static final String KEY_IMAGE_URL = "KEY_IMAGE_URL";

	public UploadPic() {
		super("UploadPic");
	}

	private DefaultHttpClient mHttpClient;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String link = intent.getStringExtra(KEY_IMAGE_URL);
		Uri uri = Uri.parse(link);
		final File image = new File(getRealPathFromURI(this, uri));
		Log.d("UploadPic",image.getAbsolutePath());
		Log.d("UploadPic",String.valueOf(image.exists()));
		//String psm = (intent.getStringExtra("psm")).toString();
		//String lang = (intent.getStringExtra("lang")).toString();
		String lang = "eng";
		String psm ="3";
		
		if (image.exists()) {
			String json = uploadUserPhoto(image);
			Log.d("json", json);
			String text = getRequest(json, lang, psm);
			Log.d("Text", text);	
			Log.d("UploadPic", "onHandleIntent2");

		}
	}

	public static class HttpClientFactory {

		private static DefaultHttpClient client;

		public synchronized static DefaultHttpClient getThreadSafeClient() {

			if (client != null)
				return client;

			client = new DefaultHttpClient();

			ClientConnectionManager mgr = client.getConnectionManager();

			HttpParams params = client.getParams();
			client = new DefaultHttpClient(new ThreadSafeClientConnManager(
					params, mgr.getSchemeRegistry()), params);

			return client;
		}
	}

	public String uploadUserPhoto(File image) {
		try {
			
			HttpClient client = HttpClientFactory.getThreadSafeClient();

			HttpPost post = new HttpPost(
					"http://api.newocr.com/v1/upload?key=fcb748ccbe4292726b70e5421b23eb72");

			MultipartEntityBuilder entityBuilder = MultipartEntityBuilder
					.create();

			// MultipartEntityBuilder entityBuilder =
			// MultipartEntityBuilder.create();
			entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

			if (image != null) {
				entityBuilder.addBinaryBody("file", image);
			}

			HttpEntity entity = entityBuilder.build();

			post.setEntity(entity);

			HttpResponse response = client.execute(post);

			HttpEntity httpEntity = response.getEntity();

			String result = EntityUtils.toString(httpEntity);

			Log.v("result", result);
			return result;

		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.d("UploadPic", "uploadUserPhoto");
		return null;

	}
	

		

	 public String getRealPathFromURI(Context context, Uri contentUri) {
	   Cursor cursor = null;
	   try { 
	     String[] proj = { MediaStore.Images.Media.DATA };
	     cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
	     int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	     cursor.moveToFirst();
	     return cursor.getString(column_index);
	   } finally {
	     if (cursor != null) {
	       cursor.close();
	     }
	   }
	 }



	public String getRequest(String json, String lang, String psm) {

		String text;

		try {
			JSONObject jsonObject = new JSONObject(json);
			JSONObject data = jsonObject.getJSONObject("data");
			String file_id = (String) data.getString("file_id");
			String pages = (String) data.getString("pages");

			String link = "http://api.newocr.com/v1/ocr?key=fcb748ccbe4292726b70e5421b23eb72&file_id="
					+ file_id
					+ "&page="
					+ pages
					+ "&lang="
					+ lang
					+ "&psm="
					+ psm;
			Log.d("get request", link);
			DefaultHttpClient hc = new DefaultHttpClient();
			ResponseHandler<String> res = new BasicResponseHandler();
			HttpGet http = new HttpGet(link);
			String json2;
			try {
				json2 = hc.execute(http, res);
				Log.d("json2",json2);

				
				JSONObject json3 = new JSONObject(json2);
				JSONObject datas = json3.getJSONObject("data");

				text = (String) datas.getString("text");

			} catch (ClientProtocolException e) {
				e.printStackTrace();
				text = "ClientProtocolException";
			} catch (IOException e) {
				e.printStackTrace();
				text = "IOException";
			}

		} catch (JSONException e1) {
			text = "JSONException";
			e1.printStackTrace();
			
		}
		Log.d("UploadPic", "getRequest");

		return text;
	}
}
